const { defineConfig } = require('@vue/cli-service')
const path = require('path')
const themePath = path.join(__dirname, './src/assets/theme.less')

module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: './', // 项目打包构建好，默认需要https协议才能够访问项目，‘./’表示可以在磁盘目录下以files协议访问项目
  css: {
    loaderOptions: {
      less: {
        // 若 less-loader 版本小于 6.0，请移除 lessOptions 这一级，直接配置选项。
        lessOptions: {
          modifyVars: {
            // 直接覆盖变量
            /* 'text-color': '#fff',
            'border-color': '#007bff' */
            // 或者可以通过 less 文件覆盖（文件路径为绝对路径）
            hack: `true; @import "${themePath}";`
          }
        }
      }
    }
  }
})
