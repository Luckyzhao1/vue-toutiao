import request from '@/utils/request'

export const getArticleListAPI = function (_page, _limit) {
  return request.get('/articles', {
    params: { // 请求参数
      _page,
      _limit
    }
  })
}
