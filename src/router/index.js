import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home/Home'
import User from '@/views/User/User'

// 将VueRouter安装为Vue的插件
Vue.use(VueRouter)
// 路由规则数组
const routes = [
  {
    path: '/',
    component: Home,
    meta: {
      isRecord: true,
      top: 0
    }
  },
  {
    path: '/user',
    component: User
  }
]
// 创建路由实例对象
const router = new VueRouter({
  routes,
  scrollBehavior (to, from, savedPosition) {
    // return 期望滚动到哪个的位置
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: to.meta.top || 0
      }
    }
  }
})

export default router
